FROM centos:8

ARG VERSION=unknown-version
ARG BUILD_DATE=unknown-date
ARG CI_COMMIT_SHA=unknown
ARG CI_BUILD_HOSTNAME
ARG CI_BUILD_JOB_NAME
ARG CI_BUILD_ID
ARG IPMI_EXPORTER_VERSION
ARG IPMI_BINARY_URL="https://github.com/soundcloud/ipmi_exporter/releases/download/v${IPMI_EXPORTER_VERSION}/ipmi_exporter-${IPMI_EXPORTER_VERSION}.linux-amd64.tar.gz"

COPY dependencies.yum.txt /tmp

RUN yum -y install $(cat /tmp/dependencies.yum.txt) && \
    yum -y update && \
    curl -L "${IPMI_BINARY_URL}" --out "/tmp/$(basename "${IPMI_BINARY_URL}")" && \
    tar xf "/tmp/$(basename "${IPMI_BINARY_URL}")" && \
    cp -f "$(find . -type f -name "ipmi_exporter")" /usr/local/bin && \
    tar tf "/tmp/$(basename "${IPMI_BINARY_URL}")" | tac | xargs rm -rf && \
    rm -f "/tmp/$(basename "${IPMI_BINARY_URL}")" && \
    yum clean all

ENTRYPOINT ["/usr/local/bin/ipmi_exporter"]

LABEL maintainer="MetaCentrum Cloud Team <cloud[at]ics.muni.cz>" \
      org.label-schema.schema-version="1.0.0-rc.1" \
      org.label-schema.vendor="Masaryk University, ICS" \
      org.label-schema.name="ipmi_exporter" \
      org.label-schema.version="$VERSION" \
      org.label-schema.build-date="$BUILD_DATE" \
      org.label-schema.build-ci-job-name="$CI_BUILD_JOB_NAME" \
      org.label-schema.build-ci-build-id="$CI_BUILD_ID" \
      org.label-schema.build-ci-host-name="$CI_BUILD_HOSTNAME" \
      org.label-schema.url="https://gitlab.ics.muni.cz/cloud/ipmi_exporter" \
      org.label-schema.vcs-url="https://gitlab.ics.muni.cz/cloud/ipmi_exporter" \
      org.label-schema.vcs-ref="$CI_COMMIT_SHA"
