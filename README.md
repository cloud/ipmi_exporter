# ipmi_exporter

[soundcloud/ipmi_exporter](https://github.com/soundcloud/ipmi_exporter) container build repository.

## How to release new version

1. [Check last upstream version.](https://github.com/soundcloud/ipmi_exporter/releases)
1. Update [CHANGELOG.md](/CHANGELOG.md) with same upstream version including `-<release-integer>` suffix to be able to release multiple versions based on same upstream code. (`<release-integer>` is integer > 0)
1. Tag repo with semver v2 compatible tag (for instance [`v1.4.0-1`](https://gitlab.ics.muni.cz/cloud/ipmi_exporter/-/tags/v1.4.0-1))
